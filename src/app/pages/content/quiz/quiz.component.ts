import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  listProduct: any[] = [];
  categories: any = ['Shirt', 'Shoes', 'Pants'];
  shipping: any = [{name: 'Express', price: 20000}, {name: 'Reguler', price: 10000}, {name: 'Economy', price: 8000}];
  showError = false;

  constructor(private fb: FormBuilder) { }

  productForm = this.fb.group({
    sku: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
    product: [null, Validators.required],
    brand: [null, Validators.required],
    category: [null, Validators.required],
    description: [null, Validators.required],
    price: [0, Validators.required],
    quantity: [0, Validators.required],
    shipment: [0, Validators.required],
    total: [0]
  });

  ngOnInit(): void {
    const a = 100;
    const b = parseFloat('100');
    const c = a + b;
    // 100100 != 200
  }

  onSubmit(): void {

    if (!this.listProduct.some((el: { sku: null | undefined }) => el.sku === this.productForm.get('sku')?.value)) {
      this.showError = false;

      const price = this.productForm.get('price').value;
      const quantity = this.productForm.get('quantity').value;
      const shipment = parseFloat(this.productForm.get('shipment').value);
      const total = (price*quantity)+shipment;
      this.productForm.controls['total'].setValue(total);

      this.listProduct.push(this.productForm.value);
      this.productForm.reset();
    } else {
      this.showError = true;
    }
    console.log(this.listProduct);
  }

}
