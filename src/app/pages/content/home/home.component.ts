import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounce, slideInLeft } from 'ngx-animate';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ConfirmationService, MessageService],
  animations: [
    trigger('bounce', [transition('* => *', useAnimation(bounce, {
      // Set the duration to 5seconds and delay to 2seconds
      params: { timing: 5, delay: 2 }
    }))]),
    trigger('slideInLeft', [transition('* => *', useAnimation(slideInLeft))]),
  ]
})
export class HomeComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  listCandidates: any = [];
  candidate: any;
  cols: any[] = [];
  // bounce: any;

  province: any = ['DKI Jakarta', 'Jawa Barat', 'Jawa Tengah'];
  jobsRole: any = ['IT', 'Finance', 'Human Resource'];
  jobExperience: any = ['>1 Years', '>3 Years', '>6 Years'];
  showError = false;

  jobForm = this.fb.group({
    name: [null, Validators.required],
    nik: [null, [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
    phone: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    birthPlace: [null, Validators.required],
    birthDate: [null, Validators.required],
    gender: [null, Validators.required],
    address: [null, Validators.required],
    position: [null, Validators.required],
    experience: [null, Validators.required],
    location: [null, Validators.required],
    salary: [null, Validators.required],
    term: [false, Validators.required],
  });

  ngOnInit(): void {

    this.cols = [
      { field: 'name', header: 'Nama' },
      { field: 'position', header: 'Posisi' },
      { field: 'salary', header: 'Gaji' }
    ];

  }

  onSubmit(): void {

    if (!this.listCandidates.some((el: { nik: null | undefined }) => el.nik === this.jobForm.get('nik') ?.value)) {
      this.showError = false;
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
      this.listCandidates.push(this.jobForm.value);
      this.messageService.add({ severity: 'info', summary: 'Success', detail: 'Record added' });
      // this.jobForm.reset();
    } else {
      this.showError = true;
    }
    console.log(this.listCandidates);
  }

  onDetail(): void {
    
  }

  onDelete(): void {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' });
      },
      reject: (type) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'Cancel Delete' });
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({ severity: 'warn', summary: 'Cancelled', detail: 'You have cancelled' });
            break;
        }
      }
    });
  }




}
