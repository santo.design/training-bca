import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  menus: any;

  ngOnInit(): void {

    this.menus = [
      {
        id: 1,
        name: 'HOME',
        routerLink: ''
      },
      {
        id: 1,
        name: 'QUIZ',
        routerLink: 'quiz'
      },
      {
        id: 1,
        name: 'CONTACT US',
        routerLink: 'contact-us'
      },
    ];

  }

}
